FROM lscr.io/linuxserver/webtop:amd64-fedora-xfce

# Build arguments/variables

ARG BUILD_DATE
ARG VERSION

ARG USER_NAME=abc
ARG GROUP_NAME=abc
ARG USER_UID=911
ARG USER_GID=911
ARG USER_HOME=/config

ARG PG_PASS=postgres
ARG PG_USER=postgres

ARG CUSTOM_DIR=/custom

# Environment variables

ENV TITLE="CHEOPS SFC"
ENV DISPLAY=:1.0

ENV MPS_VERSION=11.12.1
ENV MPS_BASE_DIR=/opt/mps
ENV MPS_INPUT_TRAY=${MPS_BASE_DIR}/mps-input-tray
ENV MPS_OUTPUT_TRAY=${MPS_BASE_DIR}/mps-output-tray
ENV MPS_LOCAL_INPUT_TRAY=${MPS_BASE_DIR}/mps-local-input-tray

ENV PATH=$MPS_BASE_DIR/mps-server-$MPS_VERSION/bin:$PATH
ENV PATH=$MPS_BASE_DIR/mps-hmi-fc-$MPS_VERSION/bin:$PATH

ENV JAVA_HOME=/etc/alternatives/java_sdk
ENV JRE_HOME=/etc/alternatives/jre

ENV M2_HOME=/usr/local/apache-maven-3.1.1
ENV M2=$M2_HOME/bin
ENV MAVEN_OPTS=""
ENV PATH=$M2:$PATH
ENV LD_LIBRARY_PATH=$M2_HOME/lib:$LD_LIBRARY_PATH

ENV PG_DATA=/var/lib/pgsql/9.3/data
ENV PG_PATH=/usr/local
ENV PATH=$PG_PATH/bin:$PATH
ENV LD_LIBRARY_PATH=$PG_PATH/lib:$LD_LIBRARY_PATH

# Image labels

LABEL build_version="CHEOPS SFC version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="CHEOPS Science Operations Centre"
LABEL sfc_version="${MPS_VERSION}"

# Copy

COPY root/ /

RUN set -eux; \
#dnf -y install sudo vim zip unzip wget; \
#
# System packages
#
#yum -y install epel-release; \
yum -y install \
    gedit \
    java-1.8.0-openjdk \
    java-1.8.0-openjdk-devel \
    libgeotiff \
    libgomp \
    sudo \
    unzip \
    vim \
    wget \
    xterm \
    zip; \
cd $CUSTOM_DIR; \
#
# Maven
#
cd maven; \
tar -xzf apache-maven-3.1.1-bin.tar.gz; \
mv apache-maven-3.1.1 $M2_HOME; \
mv settings.xml $M2_HOME/conf/; \
rm -rf apache-maven-3.1.1-bin.tar.gz; \
cd ..; \
#
# Postgresql
#
cd postgresql; \
adduser postgres; \
tar -xzf postgresql-9.3.0.tar.gz; \
yum -y install gcc make bison bison-devel flex flex-devel readline readline-devel zlib zlib-devel; \
mkdir -p $PG_PATH; \
cd postgresql-9.3.0; \
./configure --prefix $PG_PATH; \
gmake > /dev/null; \
gmake install > /dev/null; \
cd ../; \
mkdir -p $PG_DATA; \
chown -R $PG_USER:$PG_USER $PG_DATA; \
chmod go-xrw $PG_DATA; \
su $PG_USER -c "initdb -D $PG_DATA"; \
cp pg_hba.conf $PG_DATA/pg_hba.conf; \
su $PG_USER -c "pg_ctl start -l $PG_DATA/serverlog -D $PG_DATA/"; \
sleep 3; \
psql -U $PG_USER -c "ALTER USER $PG_USER PASSWORD '$PG_PASS'"; \
cd ..; \
#
# MPS
#
cd mps; \
mkdir -p ${MPS_BASE_DIR}; \
cd mps-$MPS_VERSION; \
su $PG_USER -c "createdb -U '$PG_USER' cheops_mps"; \
psql -U $PG_USER cheops_mps < ./mps_schemas.sql > /dev/null; \
# Install the MPS and SFC
yes | ./install_mps-server.bin; \
yes | ./install_mps-hmi-fc.bin; \
cp mps.global.properties $MPS_BASE_DIR/mps-server-$MPS_VERSION/config/; \
cp mps.global.properties $MPS_BASE_DIR/mps-hmi-fc-$MPS_VERSION/config/; \
# Set proper file permissions
# Change permissions of all files to -rw-r--r--
find $MPS_BASE_DIR -type f -print0 |xargs -0 chmod 644; \
# Change permissions of all directories to drwxr-xr-x
find $MPS_BASE_DIR -type d -print0 |xargs -0 chmod 755; \
# Set the executable bit on the server and sfc binaries
chmod +x $MPS_BASE_DIR/mps-server-$MPS_VERSION/bin/* $MPS_BASE_DIR/mps-hmi-fc-$MPS_VERSION/bin/* $MPS_BASE_DIR/mps-hmi-fc-$MPS_VERSION/lib/mps-hmi-fc-product/CHEOPS-SOC-MPS; \
mkdir -p /tmp/cheops-mps; \
chmod -R +wr /tmp/cheops-mps; \
# Restore a database dump of the cheops_mps database
psql -U $PG_USER -c "drop database cheops_mps;"; \
psql -U $PG_USER -c "create database cheops_mps;"; \
psql -U $PG_USER cheops_mps < ./cheops_mps.dump > /dev/null; \
cd ..; \
su $PG_USER -c "pg_ctl stop -l $PG_DATA/serverlog -D $PG_DATA/"; \
sleep 3; \
cd ..; \
#
# Clean up
#
cd $CUSTOM_DIR; \
rm -rf maven mps postgresql;

