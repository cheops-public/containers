#!/bin/bash

# Customize the desktop environment

# Remove the default desktop shortcuts
# See example usage in https://docs.xfce.org/xfce/xfconf/xfconf-query
xfconf-query -c xfce4-desktop -p /desktop-icons/file-icons/show-filesystem -s false --create --type bool
xfconf-query -c xfce4-desktop -p /desktop-icons/file-icons/show-home -s false --create --type bool
xfconf-query -c xfce4-desktop -p /desktop-icons/file-icons/show-trash -s false --create --type bool

# Change the wallpaper
xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVNC-0/workspace0/last-image -s /custom/image/Cheops_logo.png
# Change the wallpaper style to "scaled" (4)
xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVNC-0/workspace0/image-style -s 4

# Remove "Untrusted application launcher" error message when clicking
# on desktop shortcuts. See
# https://discussion.fedoraproject.org/t/installer-launcher-in-xfce-is-claimed-to-be-untrusted/80851
for f in $HOME/Desktop/*.desktop; do
    chmod +x "$f";
    gio set -t string "$f" metadata::xfce-exe-checksum "$(sha256sum "$f" | awk '{print $1}')"
done
