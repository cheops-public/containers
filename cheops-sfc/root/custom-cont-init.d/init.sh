#!/bin/bash

set -e

echo "--------------------------------------------------------------------------------"
echo "CHEOPS SFC custom files START"
echo ""

home=/config
desktop_dir=$home/Desktop
username=abc
custom_dir=/custom
init_file=$home/.config/initialized

function init() {
    # Copy all customizations
    mkdir -p $desktop_dir
    mkdir -p $home/sfc_input
    mkdir -p $home/sfc_output
    mkdir -p $home/logs
    mkdir -p $home/.config/autostart

    cp $custom_dir/home/.bashrc $home/
    cp $custom_dir/home/.bash_profile $home/

    cp -r $custom_dir/home/Desktop/* $desktop_dir/
    cp $custom_dir/home/sfc_input/* $home/sfc_input/
    cp $custom_dir/home/.config/autostart/*.desktop $home/.config/autostart/;

    # Ensure that SFC desktop shortcut is executable
    chmod +x $desktop_dir/sfc*.desktop

    # Create SFC menu item
    cp $custom_dir/home/Desktop/sfc.desktop /usr/share/applications/
    chmod -x /usr/share/applications/sfc.desktop

    # Create Gedit desktop shortcut
    cp /usr/share/applications/org.gnome.gedit.desktop $desktop_dir/;
    chmod +x $desktop_dir/org.gnome.gedit.desktop; 

    chown -R $username:$username $home;

    # Start postgres and the MPS server
    su postgres -c "pg_ctl start -l $PG_DATA/serverlog -D $PG_DATA/"
    nohup CHEOPS-SOC-MPS-SERVER &
    sleep 3

    touch $init_file
}

if [ -f $init_file ]; then
    echo "Skipping customization: already initialized"
else
    set -x
    init
    set +x
fi

echo ""
echo "CHEOPS SFC custom files END"
echo "--------------------------------------------------------------------------------"
