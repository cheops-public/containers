# CHEOPS Scheduling Feasibility Checker docker container

# Getting started

1. Install Docker

2. Start the cheops-sfc docker container by running this command:
```bash
docker run \
  -d \
  --name=cheops-sfc \
  --platform linux/amd64 \
  --security-opt seccomp=unconfined \
  -p 3000:3000 \
  -v $PWD/cheops-sfc:/config \
  --shm-size="1gb" \
  registry.gitlab.unige.ch/cheops-public/containers/cheops-sfc/cheops-sfc:0.1
```

The -v option defines a directory that will be used as the home directory (/config) inside the docker container. This directory can be used to exchange files between your computer and the docker container. Please modify `$PWD/cheops-sfc` as you see fit (note that it must be an absolute path).

The SFC is only compatible with the Intel (amd64) architecture. On systems with Apple Silicon, the docker container must run in emulation mode, wich is achieved using the `--platform linux/amd64` argument. For optimal performance on Apple Silicon, it is adviced to use macOS Sonoma and Docker v4.26.1 or higher with the 'Use Rosetta for x86/amd64 emulation on Apple Silicon' option checked in the Docker settings.

See [Parameters](https://github.com/linuxserver/docker-webtop?tab=readme-ov-file#parameters) for more information about the available parameters.


3. Open http://localhost:3000/ in a web browser to access the docker container's desktop. Click on the CHEOPS SFC icon to start the Scheduling Feasibility Checker. Template observation request files are available in the sfc_input directory to which there is a shortcut on the desktop. This directory is also available inside the directory that was passed as an argument when starting the container.

# Building locally

To build the image locally, use the following commands.

```
git clone https://gitlab.unige.ch/cheops-public/containers.git
cd containers/cheops-sfc
docker build \
  --build-arg BUILD_DATE="$(date +%Y-%m-%d)" \
  --build-arg VERSION=0.1 \
  -t cheops-sfc .
```