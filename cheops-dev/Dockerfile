FROM centos:7

# Arguments

ARG TARGETPLATFORM
ARG CONTAINER_NAME=cheopsdev

ARG USERNAME=cheops
ARG USER_UID=1000
ARG USER_GID=$USER_UID
ARG USER_HOME=/home/$USERNAME

ARG CHEOPS_SW=/opt/cheops

ARG BOOST_PATH=/usr/local

ARG GCC_PREFIX=/usr/local

ARG TEXLIVE_PATH=/usr/local
ARG TEXLIVE_SYM=/usr/local

# Environment

ENV PATH=$TEXLIVE_PATH/bin/texlive:$PATH
ENV CHEOPS_SW=$CHEOPS_SW
ENV PATH=$CHEOPS_SW/bin:$PATH
ENV PYTHONPATH=$CHEOPS_SW/python:$CHEOPS_SW/lib:$PYTHONPATH
ENV LD_LIBRARY_PATH=$CHEOPS_SW/lib:/usr/local/lib:/usr/local/lib64:$LD_LIBRARY_PATH
ENV EXT_INC_DIRS="-I/usr/local/include -I/usr/include/cfitsio -I/usr/include/eigen3"
ENV EXT_LIB_DIRS="-L/usr/local/lib -L/usr/local/lib64"
ENV PYTHONIOENCODING=UTF-8

WORKDIR /tmp

RUN set -eux; \
    work_dir=`pwd`; \
    yum clean all; \
    yum -y update; \
    #
    # Root user
    # 
    echo "export PS1='\T [\u@$CONTAINER_NAME \W]# '" >> /root/.bashrc; \
    #
    # Cheops user
    #
    groupadd --gid $USER_GID $USERNAME; \
    useradd --uid $USER_UID --gid $USER_GID --home-dir $USER_HOME --create-home $USERNAME; \
    echo "$USERNAME ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers; \
    echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers; \
    echo "export PS1='\T [\u@$CONTAINER_NAME \W]# '" >> $USER_HOME/.bashrc; \
    echo "--.mode column" >> $USER_HOME/.sqliterc; \
    echo ".headers on" >> $USER_HOME/.sqliterc; \
    # 
    # CHEOPS software area
    #
    mkdir -p $CHEOPS_SW; \
    chown $USERNAME:$USER_GID $CHEOPS_SW; \
    # 
    # Repositories
    # 
    #yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm; \
    yum -y install wget; \
    wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm; \
    yum -y localinstall epel-release-latest-7.noarch.rpm; \
    rm epel-release-latest-7.noarch.rpm; \
    #
    # System packages
    #
    yum -y group install "Development Tools"; \
    yum -y install \
        ant \
        bzip2-devel \
        cfitsio \
        cfitsio-devel \
        doxygen \
        eigen3 \
        freetype \
        freetype-devel \
        ghostscript \
        gsl \
        gsl-devel \
        java-1.7.0-openjdk \
        lapack-devel \
        lcov \
        libffi \
        libffi-devel \
        libpng \
        libpng-devel \
        libpqxx \
        libpqxx-devel \
        libxml2 \
        libxml2-devel \
        libxslt \
        libxslt-devel \
        man \
        mariadb \
        mariadb-libs \
        nano \
        nodejs \
        openblas-devel \
        openssl-devel \
        pandoc \
        perl-Digest-MD5 \
        sleep \
        sqlite \
        sqlite-devel \
        sudo \
        time \
        tkinter \
        tomcat \
        valgrind \
        vim \
        wget \
        which \
        xerces-c \
        xerces-c-devel \
        xz-devel; \
    # For ffmpeg for CHEOPSim
    # The repository does not contain an aarc64 version of ffmpeg
    if [ "$(uname -m)" = "x86_64" ]; then \
        yum -y localinstall --nogpgcheck https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm; \
        yum -y install ffmpeg ffmpeg-devel; \
    fi; \
    # libXScrnSaver odb libodb libodb-pgsql libodb-sqlite
    # postgresql-contrib postgresql-server
    #
    # GCC 4.9
    # Devtoolset-3, which contains gcc 4.9.x, is no longer available via yum 
    # from Software Collections. Compile gcc manually.
    #
    yum -y install gmp-devel mpfr-devel libmpc-devel; \
    wget http://mirror.koddos.net/gcc/releases/gcc-4.9.4/gcc-4.9.4.tar.bz2; \
    tar -xjf gcc-4.9.4.tar.bz2; \
    mkdir -p objdir; \
    cd objdir; \
    ../gcc-4.9.4/configure --prefix=$GCC_PREFIX --enable-languages=c,c++ --disable-multilib; \
    make; \
    make install; \
    cd $work_dir; \
    rm -rf objdir gcc-4.9.4.tar.bz2 gcc-4.9.4; \
    export PATH=/usr/local/bin:$PATH; \
    export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64:$LD_LIBRARY_PATH; \
    #
    # XSD
    #
    yum -y install xsd; \
    ln -s /usr/bin/xsdcxx /usr/bin/xsd; \
    #
    # wkhtmltopdf
    #
    wkhtml_pkg=wkhtmltox-0.12.6-1.centos7.$(uname -m).rpm; \
    wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/$wkhtml_pkg; \
    yum -y localinstall $wkhtml_pkg; \
    rm $wkhtml_pkg; \
    #
    # Python3
    #
    yum -y install \
        python3 \
        python3-devel \
        python3-tkinter; \
    #yum -y install python3-mod_wsgi
    python3 -m pip install --upgrade pip; \
    pip3 install --no-cache-dir --upgrade setuptools wheel; \
    pip3 install --no-cache-dir Cython==0.29.14; \
    # Use --no-build-isolation to force numpy to use Cython < 3
    # see https://github.com/numpy/numpy/issues/24330
    pip3 install --no-cache-dir --no-build-isolation numpy==1.18.1; \
    # Install pandas dependencies
    pip3 install --no-cache-dir python-dateutil==2.8.2 pytz==2023.3 numexpr==2.6.2 bottleneck==1.2.1; \
    # Use --no-build-isolation to force numpy to use Cython < 3
    pip3 install --no-cache-dir --no-build-isolation pandas==1.0.2; \
    pip3 install --no-cache-dir \
        astropy==3.0.5 \
        beautifulsoup4==4.6.3 \
        bitstring==3.1.5 \
        coverage==4.5.1 \
        Django==2.1 \
        django-extensions==2.2.8 \
        django-filter==2.2.0 \
        doxypypy==0.8.8.6 \
        gcovr==4.1 \
        generateDS==2.35.17 \
        ipdb \
        Jinja2==2.10 \
        jupyter==1.0.0 \
        jupyter-contrib-nbextensions==0.5.0 \
        jupyter-nbextensions-configurator==0.4.1 \
        lmfit==0.9.11 \
        lxml==4.2.5 \
        matplotlib==3.0.1 \
        # mysqlclient \
        nbconvert==5.6.1 \
        networkx==2.4 \
        nose==1.3.7 \
        nose-cov==1.6 \
        nosexcover==1.0.11 \
        pandoc==1.0.2 \
        plotly==4.5.4 \
        # psycopg2==2.8.4 \
        pylint==2.13.9 \
        PyPDF2==1.26.0 \
        python-dace-client \
        pytest \
        PyYAML==3.13 \
        regions==0.4 \
        scikit-image==0.14.2 \
        scipy==1.1.0 \
        Sphinx==1.7.9 \
        sqlacodegen==2.3.0 \
        SQLAlchemy==1.3.18 \
        tabulate==0.8.2; \
    # Install the remaining statsmodels dependencies
    pip3 install --no-cache-dir patsy==0.5.3; \
    # Use --no-build-isolation to force statsmodel to use Cython < 3
    pip3 install --no-cache-dir --no-build-isolation statsmodels==0.11.1; \
    # Ensure that the correct versions of jupyter-client and jupyter-console
    # have been installed as other packages may have caused them to be
    # upgraded
    pip3 install --no-cache-dir \
        jupyter-client==6.0.0 \
        jupyter-console==6.1.0 \
        seaborn==0.11.2; \
    jupyter contrib nbextension install --sys-prefix; \
    jupyter nbextension enable python-markdown/main; \
    # Change the backend in the matplotlibrc file
    sed -i 's/backend: TkAgg/backend: Agg/' `python3 -c "import matplotlib as mpl; print(mpl.matplotlib_fname())"`; \
    #
    # Boost
    #
    mkdir -p $BOOST_PATH; \
    wget http://sourceforge.net/projects/boost/files/boost/1.59.0/boost_1_59_0.tar.gz; \
    tar -xzf boost_1_59_0.tar.gz; \
    cd boost_1_59_0; \
    ./bootstrap.sh --prefix=$BOOST_PATH; \
    sed -i 's@using python : 2.7 : /usr ;@using python : 3.6 : python3 : /usr/include/python3.6m : /usr/lib64/libpython3.so ;@' project-config.jam; \
    ./b2 > b2.out; \
    ./b2 install > b2_install.out; \
    cd $work_dir; \
    rm -rf boost_1_59_0.tar.gz boost_1_59_0; \
    #
    # Texlive
    #
    wget --no-check-certificate https://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz; \
    mkdir install-tl; \
    tar -xzf install-tl-unx.tar.gz -C install-tl --strip-components=1; \
    cd install-tl; \
    texlive_profile=texlive.profile; \
    echo "# texlive.profile written on `date`" >> $texlive_profile; \
    echo "# It will NOT be updated and reflects only the" >> $texlive_profile; \
    echo "# installation profile at installation time." >> $texlive_profile; \
    echo "selected_scheme scheme-full" >> $texlive_profile; \
    echo "TEXDIR $TEXLIVE_PATH" >> $texlive_profile; \
    echo "TEXMFCONFIG ~/.texlive/texmf-config" >> $texlive_profile; \
    echo "TEXMFHOME ~/texmf" >> $texlive_profile; \
    echo "TEXMFLOCAL $TEXLIVE_PATH/texmf-local" >> $texlive_profile; \
    echo "TEXMFSYSCONFIG $TEXLIVE_PATH/texmf-config" >> $texlive_profile; \
    echo "TEXMFSYSVAR $TEXLIVE_PATH/texmf-var" >> $texlive_profile; \
    echo "TEXMFVAR ~/.texlive2021/texmf-var" >> $texlive_profile; \
    echo "binary_x86_64-linux 1" >> $texlive_profile; \
    echo "instopt_adjustpath 0" >> $texlive_profile; \
    echo "instopt_adjustrepo 1" >> $texlive_profile; \
    echo "instopt_letter 0" >> $texlive_profile; \
    echo "instopt_portable 0" >> $texlive_profile; \
    echo "instopt_write18_restricted 1" >> $texlive_profile; \
    echo "tlpdbopt_autobackup 0" >> $texlive_profile; \
    echo "tlpdbopt_backupdir tlpkg/backups" >> $texlive_profile; \
    echo "tlpdbopt_create_formats 1" >> $texlive_profile; \
    echo "tlpdbopt_desktop_integration 0" >> $texlive_profile; \
    echo "tlpdbopt_file_assocs 1" >> $texlive_profile; \
    echo "tlpdbopt_generate_updmap 0" >> $texlive_profile; \
    echo "tlpdbopt_install_docfiles 0" >> $texlive_profile; \
    echo "tlpdbopt_install_srcfiles 0" >> $texlive_profile; \
    echo "tlpdbopt_post_code 1" >> $texlive_profile; \
    echo "tlpdbopt_sys_bin $TEXLIVE_SYM/bin" >> $texlive_profile; \
    echo "tlpdbopt_sys_info $TEXLIVE_SYM/share/info" >> $texlive_profile; \
    echo "tlpdbopt_sys_man $TEXLIVE_SYM/share/man" >> $texlive_profile; \
    echo "tlpdbopt_w32_multi_user 1" >> $texlive_profile; \
    perl install-tl --profile=texlive.profile; \
    cd $work_dir; \
    mv $TEXLIVE_PATH/bin/$(uname -m)-linux $TEXLIVE_PATH/bin/texlive; \
    rm -rf install-tl-unx.tar.gz install-tl-*; \
    #
    # Clean
    # 
    yum clean all; \
    rm -rf /var/cache/yum/*;

# Port 8888: jupyter notebook
# Port 8000: django development server
EXPOSE 8888 8000

USER $USERNAME

WORKDIR $USER_HOME

