# CHEOPS development container for Visual Studio Code

The source code editor Visual Studio Code can, with the help of the Dev Container plugin, use prebuilt Docker containers as the runtime development environment.

The `cheops-dev` container was created for this purpose. The container comes preinstalled with the OS and software dependencies defined in the CHEOPS SOC reference platform and provides a quick and easy way to set up the environment for work package development.

# Getting started

If this is your first time using VS Code and/or development containers, please follow the instructions in [Getting Started](https://aka.ms/vscode-remote/containers/getting-started) to install the necessary software on your computer.

1. git clone this repository to your computer: `git clone git@gitlab.unige.ch:cheops-public/containers.git`
2. In VS Code, open your project folder.
3. Move the `.devcontainer` folder that is located inside `cheops-dev/` in the cloned repository into the project folder.
4. In the project folder, open the file `.devcontainer/devcontainer.json` and modify the version number in the "image" attribute to reflect the version of the cheops-dev docker image you wish to use.
5. Press F1 and choose **Remote-Containers: Reopen Folder in Container**.

Your project folder is now mounted inside the container and you are interacting with the container via VS Code.

See [Developing inside a Container](https://aka.ms/vscode-remote/containers/) for more details.

# Container specifications

## cheops-dev:15.0.0

| | Value |
|----------|-------|
| **Image architectures** | amd64/x86_64, arm64/aarch64 |
| **Container OS** | AlmaLinux9 |
| **Non-root user** | cheops |

| Software | Version |
|----------|-------|
| boost | 1.78.0 |
| cfitsio | 4.1.0 |
| gcc | 11.4.1 |
| python | 3.9.18 |
| svn | 1.14.1 |
| texlive | 2020 |
| xerces-c | 3.2.5 |
| xsd | 4.1.0 |

| Python package | Version |
|----------|-------|
| astropy | 6.0.0 | 
| Jinja2 | 3.1.3 | 
| ipdb | 0.13.13 | 
| lmfit | 1.2.2 | 
| lxml | 5.1.0 | 
| matplotlib | 3.8.2 | 
| notebook | 6.4.13 | 
| numpy | 1.26.3 | 
| pandas | 2.1.4 | 
| PyPDF2 | 3.0.1 | 
| regions | 0.8 | 
| scikit-image | 0.22.0 | 
| scipy | 1.11.4 | 
| seaborn | 0.13.1 | 
| Sphinx | 7.2.6 | 
| statsmodels | 0.14.1 | 
| tabulate | 0.9.0 | 

## cheops-dev:14.0.0

| | Value |
|----------|-------|
| **Image architectures** | amd64/x86_64, arm64/aarch64 |
| **Container OS** | Centos7 |
| **Non-root user** | cheops |

| Software | Version |
|----------|-------|
| boost | 1.59.0 |
| cfitsio | 3.370 |
| gcc | 4.9.4 |
| python | 3.6.8 |
| svn | 1.7.14 |
| texlive | 2023 |
| xerces-c | 3.1.1 |
| xsd | 4.0.0 |

| Python package | Version |
|----------|-------|
| astropy | 3.0.5 | 
| Jinja2 | 2.10 | 
| ipdb | 0.13.13 | 
| lmfit | 0.9.11 | 
| lxml | 4.9.3 | 
| matplotlib | 3.0.1 | 
| notebook | 6.4.10 | 
| numpy | 1.18.1 | 
| pandas | 1.0.2 | 
| PyPDF2 | 1.26.0 | 
| regions | 0.4 | 
| scikit-image | 0.14.2 | 
| scipy | 1.1.0 | 
| seaborn | 0.11.2 | 
| Sphinx | 1.7.9 | 
| statsmodels | 0.11.1 | 
| tabulate | 0.8.2 | 
